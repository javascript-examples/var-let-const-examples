/*jslint es6:true*/
//VAR EXAMPLES
//1-)
/*function level scoping of var*/
var l = 10;
for (var i=0;i<l;i++){
  console.log(i);
}
//"i" is accessible outside of the for loop it was defined in (hoisting)
console.log(i);



//2-)
/*hoisting of var declared inside for loop*/
var l = 10;
for (var i=0;i<l;i++){
  var inside = i;
  console.log(inside);
}
//"inside" is available outside of the for loop it was declared in (hoisting)
console.log(inside);



//3-) Be careful here
var myList = [1,2,3,4,5];
myList.forEach(function(val,i,src){
  console.log(val);
});
//val is not reachable here because it was defined inside a function and funtion level
//scoping will keep the declaration inside that funtion
console.log(val);//here "val" is undefined



//4-) 2 different "val" variables defined inside 2 different scopes
var val = "hi there";
var myList = [1,2,3,4,5];
myList.forEach(function(val,i,src){
  console.log(val);
});
console.log(val);



//LET EXAMPLES
/*
variables declared with "let" keyword will have block-level scoping
*/
//1-)
let len = 10;
for(let index=0;index<len;index++){
  console.log(index);
}
//console.log(index);//index is not defined. no hoisting using "let"



//2-)
for(let index=0;index<len;index++){
  let inside2 = index;
  console.log(inside2);
}
//console.log(inside2);//inside 2 is not defined. no hoisting using "let"



//3-)
let val2 = "hello mars";
let myList2 = [1,2,3,4,5];

myList2.forEach(function(val2, index, src){
  console.log(val2);
});
console.log(val2);//prints "hello mars". we have 2 different val2 variables



//4-)
let outside = "serdar";
let myList3 = [1,2,3,4,5];

myList3.forEach(function(val,index,src){
  outside += val;
  console.log(val);
});
//prints serdar12345 - block scoped let variables are reachable from within child blocks
console.log(outside);



//CONST EXAMPLES
//1-)
const len2 = 5;
/*
as soon as the loop tries to increment i we get this -> TypeError: Assignment to constant variable.
const variables can't be re-assigned
a const variable can't have its value reference changed
*/
// for(const i=0;i<len2;i++){
//   console.log(i);
// }



//2-)
const val3 = "jupiter";//block-scoped val3
const myList4 = [1,2,3,4,5];
myList4.forEach(function(val3, index, src){
  console.log(val3);//here val3 is function-scoped. we have 2 different variables
});
console.log(val3);//prints "jupiter". we 



//3-)
const outside2 = "blackhole";
const myList5 = [1,2,3,4,5];
myList5.forEach(function(val,index,src){
  //outside2 += val;//TypeError: Assignment to constant variable
  console.log(val);
});
console.log(outside2);



//4-)
const myList6 = [1,2,3,4,5];
myList6.forEach(function(val,index,src){
  //this will work because each time the loop executes "inner" 
  //variable is declared again. we are not re-assigning
  const inner = "neptun+" + val;
  console.log(inner);
});



//5-)Be careful here
/*
const variables cannot be re-assigned but 
the contents of const objects/arrays can be modified/mutated
*/
const myObj = {"planet":"earth"};
const myList7 = [1,2,3,4,5];
myList7.forEach(function(val,index,src){
  myObj.planet = myObj.planet+val;
});
console.log(myObj);//prints -> planet: "earth12345"
